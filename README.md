# Assignment A3

Copy Dockerfile to your Project root directory.
To build the Docker Image, switch to the folder where Dockerfile is and run:

`docker build -t mynodeapp:latest .`

To start your Docker Image, run:

`docker run -d -p3000:3000 mynodeapp:latest`

To access from browser:
http://localhost:3000
